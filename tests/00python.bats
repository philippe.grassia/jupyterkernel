#!/usr/bin/bats

function setup() {
   echo "versions of nbconvert"
   python3 -c "import nbconvert; print (nbconvert.__version__)"

}


function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }


@test "python 2.7 is installed" {
   run python2 -c "import sys ; print(sys.version)"
   version_gt "$output" "2.7"
}

@test "python 3.5+ is installed" {
   run python3 -c "import sys ; print(sys.version)"
   version_gt "$output" "3.5"
}

@test "jupyter notebook 5.7+ is installed" {
   run /usr/bin/jupyter notebook --version
   version_gt "$output" "5.7"
}

@test "jupyter nbconvert 5.4+ is installed" {
   run python3 -c "import nbconvert; print (nbconvert.__version__)"
   version_gt "$output" "5.4"
}

@test "numpy is installed" {
   run  python -c "import numpy as np ; print np.__version__"
   run  python3 -c "import numpy as np ; print( np.__version__)"
   grep -qE '1\.[1-9][0-9]\.[0-9]+' <<< $output
   echo "1.12.1"
}

@test "scipy is installed" {
   run python -c "import scipy as sp ; print( sp.__version__)"
   run python3 -c "import scipy as sp ; print( sp.__version__)"
   echo  "0.18.1"
}

@test "maptplotlib 2.x installed" {
    run python -c "import matplotlib as mpl; print( mpl.__version__)"
    run python3 -c "import matplotlib as mpl; print( mpl.__version__)"
    run grep -qE '2\.[0-9]+\.[0-9]+' <<< $output
    echo "2.0.0"
}
