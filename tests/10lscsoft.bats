#!/usr/bin/bats -t

function version_gt() { test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; }


@test "lscsoft repo for stretch is installed" {
   # skip if not running on stretch
   lsb_release -d | grep -q stretch || skip " running $(lsb_release -d| grep -o '\W+$')"
   echo "lscsoft.list: "
   cat /etc/apt/sources.list.d/lscsoft.list
   # 2 entries on lscsoft.list for stretch
   run bash -c "grep stretch /etc/apt/sources.list.d/lscsoft.list| wc -l"
   echo "output: "$output
   echo "status: "$status
   [ "$output" -eq 2 ]
   # nothing that is not stretch not comment
   run bash -c "grep -v 'stretch\|^#'  /etc/apt/sources.list.d/lscsoft.list| wc -l"
   [ "$output" -eq 0 ]
}

@test "lscsoft repo for buster is installed" {
   # skip if not running on buster
   lsb_release -d | grep buster || skip " running $(lsb_release -d| awk '{print $2}') "
   echo "lscsoft.list: "
   cat /etc/apt/sources.list.d/lscsoft.list
   # 2 entries on lscsoft.list for buster
   run bash -c "grep buster /etc/apt/sources.list.d/lscsoft.list | wc -l"
   [ "$output" -eq 2 ]
   # nothing that is not buster not comment
   run bash -c "grep -v 'buster\|^#'  /etc/apt/sources.list.d/lscsoft.list | wc -l"
   [ "$output" -eq 0 ]
}

@test "nds2 client 0.16+ is installed on python2"  {
   # skip if not running on stretch (buster is dropiing python2)
   lsb_release -d | grep stretch || skip " running $(lsb_release -d | grep '\W+$'), not testing python-nds2 on buster "
   run python2 -c "import nds2; print nds2.version()"
   version_gt "$output" "0.16"
}

@test "nds2 client 0.16+ is installed on python3"  {
    # skip buster for now, package not ready, sitll v 0.15.3
    #lsb_release -d | grep buster && skip " running $(lsb_release -d| grep '\W+$'), not testing python3-nds2 on buster "
    run python3 -c "import nds2; print(nds2.version())"
    version_gt "$output" = "0.15.2"
}

@test "gwpy installed in python2 and python3" {
    run python2 -c "import gwpy; print gwpy.__version__"  2>/dev/null
    version_gt "$output"  "0.14"
    run python -c "import gwpy; print(gwpy.__version__)"
    version_gt "$output" "0.14"
}

@test "lal is installed in python2" {
   # skip if not running on buster
   lsb_release -d | grep buster || skip " running $(lsb_release -d), not testing python-nds2 on buster "

    run python2  -c "import lal; print(lal.VERSION)"
    version_gt  "$output" "6.19"
}

@test "lal is installed in python3" {
    run python3  -c "import lal; print(lal.VERSION)"
    version_gt "$output" "6.19"
}
