from __future__ import print_function
import unittest
import sys
import numpy as np

def versionStringCompare(a,b):
    """
    compare version strings in the form MAJ.min.rev
    :param a:
    :param b:
    :return: -1 if a<b, 0 if a==b ; 1 if a>b
    """
    a_maj, a_min, a_rev = [ int(tok) for tok in a.split('.')[:3]]
    b_maj, b_min, b_rev = [ int(tok) for tok in b.split('.')[:3]]

    if a_maj>b_maj:
        return 1
    elif a_maj<b_maj:
        return -1
    else:
        if a_min>b_min:
            return 1
        elif a_min<b_min:
            return -1
        else:
            if a_rev>b_rev:
                return 1
            elif a_rev<b_rev:
                return -1
    return 0

class TestLoadableModules(unittest.TestCase):
    """
    check that a key set of modules are installed optionnaly with a minimum viable version
    """

    def test_numpy_astropy(self):
        try:
            import numpy as np
            import astropy
        except ImportError:
            self.fail("test_numpy_astropy(): failed to import numpy or astropy.")
        self.assertFalse(versionStringCompare(np.__version__, "1.12.0") < 0,
                         "numpy version too old {}".format(np.__version__))

        self.assertFalse(versionStringCompare(astropy.__version__ + '.0', "1.2.0") < 0,
                         "astropy version too old {}".format(astropy.__version__))

    def test_gwpy(self):
        try:
            import gwpy
        except ImportError:
            self.fail("test_gwpy(): failed to import gwpy")
        self.assertFalse(versionStringCompare(gwpy.__version__, "0.9.0") < 0,
                         "gwpy version too old {}".format(gwpy.__version__))

    @unittest.skipIf(sys.version_info.major>2, "nds2 not yet released for python3")
    def test_nds2(self):
        try:
            import nds2
        except ImportError:
            self.fail("test_nds2(): failed to import nds2")
        self.assertFalse(versionStringCompare(nds2.version(), "0.15.2") < 0,
                         "nds2 client version too old {}".format(nds2.version()))

    def test_htcondor(self):
        try:
            import htcondor
        except ImportError:
            self.fail("test_htcondor): failed to import htcondor")
        version=htcondor.version().split()[1]
        self.assertFalse(versionStringCompare(version, "8.4.4") < 0,
                         "htcondor version too old {}".format(version))

if __name__ == '__main__':
    unittest.main()
