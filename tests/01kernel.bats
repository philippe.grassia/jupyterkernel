#!/usr/bin/bats -t


@test "jupyter kernels are installed" {
   run python3 -c "import nbconvert; print (nbconvert.__version__)"
   run jupyter kernelspec list
   [[ "$output" =~ "python2" ]]
   [[ "$output" =~ "python3" ]]
}


@test "python2 kernel runs trivial notebook" {
   run jupyter nbconvert --stdout --execute --to=rst  python2.ipynb
   run true
}

@test "python3 kernel runs trivial notebook" {
    run  jupyter nbconvert --stdout --execute --to=rst  python3.ipynb
    run true
}
