#!/usr/bin/env bash

## install testing module
pip3 install jupyter_kernel_test

## or maybe it would be better to use nbval
pip3 install nbval
## from nbval.kernel import RunninggKernel

python3 kernel_test.py
