#!make -f

.PHONY: stretch openscience *


stretch: stretch/Dockerfile
	docker build --pull -t jupyterkernel-stretch -f stretch/Dockerfile .

buster: buster/Dockerfile
	docker build --no-cache --pull -t jupyterkernel-buster -f buster/Dockerfile .

openscience: openscience/Dockerfile
	docker build --pull -t jupyterkernel-openscience -f openscience/DockerFile .

ushell:
	docker run -it --rm -v $$PWD:/Project -u root  jupyterkernel-stretch  /bin/bash

gitlab-ci:
	awk -F':' '/^[a-zA-z0-9]/ && !/variables|stages/ {print $$1}' .gitlab-ci.yml  | xargs -n 1 -t gitlab-runner exec shell

singularity:
	docker build --pull --rm -t containers.ligo.org/philippe.grassia/jupyterkernel-stretch -f stretch/Dockerfile .
	echo "Pushing singularity under $$(date +%y.%m.%d-stretch)"
	sudo singularity build -s /opt/jupyterkernel/$$(date +%y.%m.%d-stretch) docker://containers.ligo.org/philippe.grassia/jupyterkernel:latest
